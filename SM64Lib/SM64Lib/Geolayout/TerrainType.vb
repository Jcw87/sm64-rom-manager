﻿Imports System.IO
Imports SM64Lib.Geolayout.Script, SM64Lib.Geolayout.Script.Commands
Imports SM64Lib.Levels

Namespace Global.SM64Lib.Geolayout

    Public Enum TerrainTypes
        NoramlA = &H0
        NoramlB
        SnowTerrain
        SandTerrain
        BigBoosHount
        WaterLevels
        SlipperySlide
    End Enum

End Namespace
