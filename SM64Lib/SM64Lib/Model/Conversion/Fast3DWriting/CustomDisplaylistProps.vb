﻿Imports SM64Lib.Model.Fast3D.DisplayLists

Namespace Model.Conversion.Fast3DWriting

    Public Class CustomDisplaylistProps

        Public Property ID As Integer = 0
        Public Property DisplaylistType As DisplayListType = DisplaylistType.Solid

    End Class

End Namespace
