﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Tab_TextManager
    Inherits System.Windows.Forms.UserControl

    'UserControl überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Tab_TextManager))
        Me.GroupPanel_TM_DialogProps = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX21 = New DevComponents.DotNetBar.LabelX()
        Me.ComboBoxEx_TM_DialogPosY = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ComboItem10 = New DevComponents.Editors.ComboItem()
        Me.ComboItem8 = New DevComponents.Editors.ComboItem()
        Me.IntegerInput_TM_DialogSize = New DevComponents.Editors.IntegerInput()
        Me.ComboBoxEx_TM_DialogPosX = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ComboItem5 = New DevComponents.Editors.ComboItem()
        Me.ComboItem6 = New DevComponents.Editors.ComboItem()
        Me.ComboItem7 = New DevComponents.Editors.ComboItem()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.TabStrip_TextTable = New DevComponents.DotNetBar.TabStrip()
        Me.ListViewEx_TM_TableEntries = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Line_TM_Green = New DevComponents.DotNetBar.Controls.Line()
        Me.Line_TM_Warning1 = New DevComponents.DotNetBar.Controls.Line()
        Me.Line_TM_Warning2 = New DevComponents.DotNetBar.Controls.Line()
        Me.TextBoxX_TM_TextEditor = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Bar_AddRemoveItems = New DevComponents.DotNetBar.Bar()
        Me.ButtonItem_AddTextItem = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_RemoveTextItem = New DevComponents.DotNetBar.ButtonItem()
        Me.GroupPanel_TM_DialogProps.SuspendLayout()
        CType(Me.IntegerInput_TM_DialogSize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bar_AddRemoveItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel_TM_DialogProps
        '
        resources.ApplyResources(Me.GroupPanel_TM_DialogProps, "GroupPanel_TM_DialogProps")
        Me.GroupPanel_TM_DialogProps.BackColor = System.Drawing.Color.Transparent
        Me.GroupPanel_TM_DialogProps.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel_TM_DialogProps.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel_TM_DialogProps.Controls.Add(Me.LabelX22)
        Me.GroupPanel_TM_DialogProps.Controls.Add(Me.LabelX21)
        Me.GroupPanel_TM_DialogProps.Controls.Add(Me.ComboBoxEx_TM_DialogPosY)
        Me.GroupPanel_TM_DialogProps.Controls.Add(Me.IntegerInput_TM_DialogSize)
        Me.GroupPanel_TM_DialogProps.Controls.Add(Me.ComboBoxEx_TM_DialogPosX)
        Me.GroupPanel_TM_DialogProps.Controls.Add(Me.LabelX18)
        Me.GroupPanel_TM_DialogProps.DisabledBackColor = System.Drawing.Color.Empty
        Me.GroupPanel_TM_DialogProps.Name = "GroupPanel_TM_DialogProps"
        '
        '
        '
        Me.GroupPanel_TM_DialogProps.Style.BackColorGradientAngle = 90
        Me.GroupPanel_TM_DialogProps.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel_TM_DialogProps.Style.BorderBottomWidth = 1
        Me.GroupPanel_TM_DialogProps.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel_TM_DialogProps.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel_TM_DialogProps.Style.BorderLeftWidth = 1
        Me.GroupPanel_TM_DialogProps.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel_TM_DialogProps.Style.BorderRightWidth = 1
        Me.GroupPanel_TM_DialogProps.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel_TM_DialogProps.Style.BorderTopWidth = 1
        Me.GroupPanel_TM_DialogProps.Style.CornerDiameter = 4
        Me.GroupPanel_TM_DialogProps.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel_TM_DialogProps.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel_TM_DialogProps.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel_TM_DialogProps.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel_TM_DialogProps.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel_TM_DialogProps.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelX22
        '
        '
        '
        '
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        resources.ApplyResources(Me.LabelX22, "LabelX22")
        Me.LabelX22.Name = "LabelX22"
        '
        'LabelX21
        '
        '
        '
        '
        Me.LabelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        resources.ApplyResources(Me.LabelX21, "LabelX21")
        Me.LabelX21.Name = "LabelX21"
        '
        'ComboBoxEx_TM_DialogPosY
        '
        Me.ComboBoxEx_TM_DialogPosY.DisplayMember = "Text"
        Me.ComboBoxEx_TM_DialogPosY.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboBoxEx_TM_DialogPosY.ForeColor = System.Drawing.Color.Black
        Me.ComboBoxEx_TM_DialogPosY.FormattingEnabled = True
        resources.ApplyResources(Me.ComboBoxEx_TM_DialogPosY, "ComboBoxEx_TM_DialogPosY")
        Me.ComboBoxEx_TM_DialogPosY.Items.AddRange(New Object() {Me.ComboItem10, Me.ComboItem8})
        Me.ComboBoxEx_TM_DialogPosY.Name = "ComboBoxEx_TM_DialogPosY"
        Me.ComboBoxEx_TM_DialogPosY.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        '
        'ComboItem10
        '
        resources.ApplyResources(Me.ComboItem10, "ComboItem10")
        '
        'ComboItem8
        '
        resources.ApplyResources(Me.ComboItem8, "ComboItem8")
        '
        'IntegerInput_TM_DialogSize
        '
        '
        '
        '
        Me.IntegerInput_TM_DialogSize.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.IntegerInput_TM_DialogSize.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IntegerInput_TM_DialogSize.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        resources.ApplyResources(Me.IntegerInput_TM_DialogSize, "IntegerInput_TM_DialogSize")
        Me.IntegerInput_TM_DialogSize.MaxValue = 255
        Me.IntegerInput_TM_DialogSize.MinValue = 1
        Me.IntegerInput_TM_DialogSize.Name = "IntegerInput_TM_DialogSize"
        Me.IntegerInput_TM_DialogSize.ShowUpDown = True
        Me.IntegerInput_TM_DialogSize.Value = 1
        '
        'ComboBoxEx_TM_DialogPosX
        '
        Me.ComboBoxEx_TM_DialogPosX.DisplayMember = "Text"
        Me.ComboBoxEx_TM_DialogPosX.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboBoxEx_TM_DialogPosX.ForeColor = System.Drawing.Color.Black
        Me.ComboBoxEx_TM_DialogPosX.FormattingEnabled = True
        resources.ApplyResources(Me.ComboBoxEx_TM_DialogPosX, "ComboBoxEx_TM_DialogPosX")
        Me.ComboBoxEx_TM_DialogPosX.Items.AddRange(New Object() {Me.ComboItem5, Me.ComboItem6, Me.ComboItem7})
        Me.ComboBoxEx_TM_DialogPosX.Name = "ComboBoxEx_TM_DialogPosX"
        Me.ComboBoxEx_TM_DialogPosX.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        '
        'ComboItem5
        '
        resources.ApplyResources(Me.ComboItem5, "ComboItem5")
        '
        'ComboItem6
        '
        resources.ApplyResources(Me.ComboItem6, "ComboItem6")
        '
        'ComboItem7
        '
        resources.ApplyResources(Me.ComboItem7, "ComboItem7")
        '
        'LabelX18
        '
        '
        '
        '
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        resources.ApplyResources(Me.LabelX18, "LabelX18")
        Me.LabelX18.Name = "LabelX18"
        '
        'TabStrip_TextTable
        '
        resources.ApplyResources(Me.TabStrip_TextTable, "TabStrip_TextTable")
        Me.TabStrip_TextTable.AutoSelectAttachedControl = True
        Me.TabStrip_TextTable.CanReorderTabs = False
        Me.TabStrip_TextTable.CloseButtonPosition = DevComponents.DotNetBar.eTabCloseButtonPosition.Right
        Me.TabStrip_TextTable.CloseButtonVisible = True
        Me.TabStrip_TextTable.Cursor = System.Windows.Forms.Cursors.Default
        Me.TabStrip_TextTable.ForeColor = System.Drawing.Color.Black
        Me.TabStrip_TextTable.Name = "TabStrip_TextTable"
        Me.TabStrip_TextTable.SelectedTab = Nothing
        Me.TabStrip_TextTable.Style = DevComponents.DotNetBar.eTabStripStyle.Metro
        Me.TabStrip_TextTable.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Top
        '
        'ListViewEx_TM_TableEntries
        '
        resources.ApplyResources(Me.ListViewEx_TM_TableEntries, "ListViewEx_TM_TableEntries")
        Me.ListViewEx_TM_TableEntries.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.ListViewEx_TM_TableEntries.Border.Class = "ListViewBorder"
        Me.ListViewEx_TM_TableEntries.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ListViewEx_TM_TableEntries.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader13, Me.ColumnHeader14})
        Me.ListViewEx_TM_TableEntries.DisabledBackColor = System.Drawing.Color.Empty
        Me.ListViewEx_TM_TableEntries.FocusCuesEnabled = False
        Me.ListViewEx_TM_TableEntries.ForeColor = System.Drawing.Color.Black
        Me.ListViewEx_TM_TableEntries.FullRowSelect = True
        Me.ListViewEx_TM_TableEntries.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.ListViewEx_TM_TableEntries.HideSelection = False
        Me.ListViewEx_TM_TableEntries.Name = "ListViewEx_TM_TableEntries"
        Me.ListViewEx_TM_TableEntries.UseCompatibleStateImageBehavior = False
        Me.ListViewEx_TM_TableEntries.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        resources.ApplyResources(Me.ColumnHeader1, "ColumnHeader1")
        '
        'ColumnHeader13
        '
        resources.ApplyResources(Me.ColumnHeader13, "ColumnHeader13")
        '
        'ColumnHeader14
        '
        resources.ApplyResources(Me.ColumnHeader14, "ColumnHeader14")
        '
        'Line_TM_Green
        '
        resources.ApplyResources(Me.Line_TM_Green, "Line_TM_Green")
        Me.Line_TM_Green.BackColor = System.Drawing.Color.Transparent
        Me.Line_TM_Green.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Line_TM_Green.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash
        Me.Line_TM_Green.ForeColor = System.Drawing.Color.YellowGreen
        Me.Line_TM_Green.Name = "Line_TM_Green"
        Me.Line_TM_Green.VerticalLine = True
        '
        'Line_TM_Warning1
        '
        resources.ApplyResources(Me.Line_TM_Warning1, "Line_TM_Warning1")
        Me.Line_TM_Warning1.BackColor = System.Drawing.Color.Transparent
        Me.Line_TM_Warning1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Line_TM_Warning1.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash
        Me.Line_TM_Warning1.ForeColor = System.Drawing.Color.Orange
        Me.Line_TM_Warning1.Name = "Line_TM_Warning1"
        Me.Line_TM_Warning1.VerticalLine = True
        '
        'Line_TM_Warning2
        '
        resources.ApplyResources(Me.Line_TM_Warning2, "Line_TM_Warning2")
        Me.Line_TM_Warning2.BackColor = System.Drawing.Color.Transparent
        Me.Line_TM_Warning2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Line_TM_Warning2.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash
        Me.Line_TM_Warning2.ForeColor = System.Drawing.Color.Red
        Me.Line_TM_Warning2.Name = "Line_TM_Warning2"
        Me.Line_TM_Warning2.VerticalLine = True
        '
        'TextBoxX_TM_TextEditor
        '
        resources.ApplyResources(Me.TextBoxX_TM_TextEditor, "TextBoxX_TM_TextEditor")
        Me.TextBoxX_TM_TextEditor.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TextBoxX_TM_TextEditor.Border.Class = "TextBoxBorder"
        Me.TextBoxX_TM_TextEditor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX_TM_TextEditor.DisabledBackColor = System.Drawing.Color.White
        Me.TextBoxX_TM_TextEditor.ForeColor = System.Drawing.Color.Black
        Me.TextBoxX_TM_TextEditor.Name = "TextBoxX_TM_TextEditor"
        Me.TextBoxX_TM_TextEditor.PreventEnterBeep = True
        Me.TextBoxX_TM_TextEditor.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty
        '
        'Bar_AddRemoveItems
        '
        Me.Bar_AddRemoveItems.AntiAlias = True
        resources.ApplyResources(Me.Bar_AddRemoveItems, "Bar_AddRemoveItems")
        Me.Bar_AddRemoveItems.IsMaximized = False
        Me.Bar_AddRemoveItems.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem_AddTextItem, Me.ButtonItem_RemoveTextItem})
        Me.Bar_AddRemoveItems.Name = "Bar_AddRemoveItems"
        Me.Bar_AddRemoveItems.Stretch = True
        Me.Bar_AddRemoveItems.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Bar_AddRemoveItems.TabStop = False
        '
        'ButtonItem_AddTextItem
        '
        Me.ButtonItem_AddTextItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_AddTextItem.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far
        Me.ButtonItem_AddTextItem.Name = "ButtonItem_AddTextItem"
        Me.ButtonItem_AddTextItem.Symbol = "57669"
        Me.ButtonItem_AddTextItem.SymbolColor = System.Drawing.Color.FromArgb(CType(CType(82, Byte), Integer), CType(CType(124, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ButtonItem_AddTextItem.SymbolSet = DevComponents.DotNetBar.eSymbolSet.Material
        Me.ButtonItem_AddTextItem.SymbolSize = 12.0!
        '
        'ButtonItem_RemoveTextItem
        '
        Me.ButtonItem_RemoveTextItem.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_RemoveTextItem.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far
        Me.ButtonItem_RemoveTextItem.Name = "ButtonItem_RemoveTextItem"
        Me.ButtonItem_RemoveTextItem.Symbol = "57676"
        Me.ButtonItem_RemoveTextItem.SymbolColor = System.Drawing.Color.FromArgb(CType(CType(150, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ButtonItem_RemoveTextItem.SymbolSet = DevComponents.DotNetBar.eSymbolSet.Material
        Me.ButtonItem_RemoveTextItem.SymbolSize = 12.0!
        '
        'Tab_TextManager
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Bar_AddRemoveItems)
        Me.Controls.Add(Me.GroupPanel_TM_DialogProps)
        Me.Controls.Add(Me.TabStrip_TextTable)
        Me.Controls.Add(Me.ListViewEx_TM_TableEntries)
        Me.Controls.Add(Me.Line_TM_Green)
        Me.Controls.Add(Me.Line_TM_Warning1)
        Me.Controls.Add(Me.Line_TM_Warning2)
        Me.Controls.Add(Me.TextBoxX_TM_TextEditor)
        Me.Name = "Tab_TextManager"
        Me.GroupPanel_TM_DialogProps.ResumeLayout(False)
        CType(Me.IntegerInput_TM_DialogSize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bar_AddRemoveItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupPanel_TM_DialogProps As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ComboBoxEx_TM_DialogPosY As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ComboItem10 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem8 As DevComponents.Editors.ComboItem
    Friend WithEvents IntegerInput_TM_DialogSize As DevComponents.Editors.IntegerInput
    Friend WithEvents ComboBoxEx_TM_DialogPosX As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ComboItem5 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem6 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem7 As DevComponents.Editors.ComboItem
    Friend WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TabStrip_TextTable As DevComponents.DotNetBar.TabStrip
    Friend WithEvents ListViewEx_TM_TableEntries As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader13 As ColumnHeader
    Friend WithEvents ColumnHeader14 As ColumnHeader
    Friend WithEvents Line_TM_Green As DevComponents.DotNetBar.Controls.Line
    Friend WithEvents Line_TM_Warning1 As DevComponents.DotNetBar.Controls.Line
    Friend WithEvents Line_TM_Warning2 As DevComponents.DotNetBar.Controls.Line
    Friend WithEvents TextBoxX_TM_TextEditor As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Bar_AddRemoveItems As DevComponents.DotNetBar.Bar
    Friend WithEvents ButtonItem_AddTextItem As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem_RemoveTextItem As DevComponents.DotNetBar.ButtonItem
End Class
